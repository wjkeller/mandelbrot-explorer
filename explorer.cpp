// explorer.cpp - ECS175 Obermaier HW 1
// Author: William Keller (#996964933)

#include <GL/glew.h>
#include <GL/glut.h>
#include <GL/glu.h>
#include <GL/gl.h>

#include <ctime>
#include <string>
#include <fstream>
#include <streambuf>
#include <iostream>

#include <sys/time.h> // hopefully the same, I deved on OSX so had a bunch of repeat headers. This is mostly cleaned up.

using namespace std;

/*************************

Hello! Welcome to my homework. It got a little unruly, but util funcs are at the top,
glut handlers are in the middle, and animation stuff is at the bottom, as well as strewn inexorably through everything else.

This was actually pretty clean until the animation came in :(.
The time stuff using gettime isn't actually generating a particularly meaningful metric, but it works for what I need (a reference)
	and has higher precision than clock(). The downside is you have to wait for up to a second for an animation to start if you're unlucky.

The program maintains a lot of state, especially w.r.t the animation, as the shader only really has an insantaneous view of the world,
	but all of the real heavy lifting, including shape outlining, is handled inside the shader for extra flexibility.

I hope you enjoy my project; I put a lot of effort into it!

William Keller (#996964933)
*************************/

// some OGL globals
GLuint shader_program;
int viewport_width = 300;
int viewport_height = 200;

// program state
float pan_x = -1.0;
float pan_y = 0.0;
float scale = 1.0;
float animating = 0.0; // just a flag
suseconds_t animation_time_base = 0.0; // these HAVE to be susecond_t's! otherwise some types are inferred wrong and it breaks
suseconds_t animation_starting_time = 0.0;
clock_t last_frame_time = 0.0;
bool zooming = false;
bool resetting = false;
float zoom_stop = 1.0;
int queued_animation = -1;

// forward decs of some util funcs
string get_contents(const char* filename);
GLint setup_shader(const char* filename);
bool setup_graphics();
void setup_viewport();

// forward decs of handlers
void reshape_handler(GLint new_x, GLint new_y);
void display_handler();
void mouse_handler(GLint button, GLint state, GLint pos_x, GLint pos_y);
void keyboard_handler(unsigned char key, GLint pos_x, GLint pos_y);
void keyboard_special_handler(int key, GLint pos_x, GLint pos_y);
void idle_handler();

// and of some places to zoom into!
void loc1();
void loc2();
void loc3();
void reset(); // center camera

int main(int argc, char **argv) {
	// init glut and let it eat the args it wants to
	glutInit(&argc, argv);

	// record the starting time as an animation reference point
	timeval start;
	gettimeofday(&start, NULL);
	animation_time_base = start.tv_sec + start.tv_usec / 1000000.0;
	last_frame_time = animation_time_base;

	if(setup_graphics() != true) {
		cout << "Exiting with errors." << endl;
		return 1;
	}

	// good to go! enter main loop
	glutMainLoop();
}

////////////////////////////////////////////// some utility functions
string get_contents(const char* filename) {
	// stream to string conversion nabbed from stack overflow
	ifstream fh(filename);
	string str((istreambuf_iterator<char>(fh)), istreambuf_iterator<char>());
	return str;
}

GLint setup_shader(const char* filename) {
	GLint shader = glCreateShader(GL_FRAGMENT_SHADER);
	string source = get_contents(filename);

	// ugly, have to figure out the proper cast
	const char *wrap[1];
	wrap[0] = source.c_str();

	// try to compile shader
	glShaderSource(shader, 1, wrap, NULL); // 1, NULL means take a 1-length array of cstrings
	glCompileShader(shader);

	// check if it worked
	GLint status;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);

	if(status != GL_TRUE) {
		// shader error. print it and return with failure.
		GLchar error_log[1024]; // hopefully long enough
		GLsizei length; // of the copied message

		glGetShaderInfoLog(shader, 1024, &length, error_log); // copy error to error_log
		cout << "Shader did not compile correctly." << endl;
		cout << error_log << endl;

		return -1;
	}

	// success!
	return shader;
}

bool setup_graphics() {
	// start by setting up glut
	glutInitWindowSize(viewport_width, viewport_height);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB | GLUT_DEPTH); // forgetting GLUT_DEPTH earned me a segfault!

    // make our window
    glutCreateWindow("Mandelbrot Explorer");

    // bind basic handlers
    glutDisplayFunc(display_handler);
	glutReshapeFunc(reshape_handler);
	glutIdleFunc(idle_handler);

	// bind input handlers
	glutMouseFunc(mouse_handler);
	glutKeyboardFunc(keyboard_handler);
	glutSpecialFunc(keyboard_special_handler);

	// load anything else that needs loading. this seems to cover it.
	glewInit();

	// now we can set up our shaders
	shader_program = glCreateProgram(); // holds collection of shaders
	GLint mandel_frag_shader = setup_shader("frag_mandelbrot.glsl");

	// check shader compiled okay
	if(mandel_frag_shader == -1) {
		cout << "Aborting due to shader compilation error." << endl;
		return false; // exit with error
	}

	// attach the shader to the program, then link and activate it.
	glAttachShader(shader_program, mandel_frag_shader);
	glLinkProgram(shader_program);

	// make sure it linked
	GLint status;
	glGetProgramiv(shader_program, GL_LINK_STATUS, &status);

	if(status == GL_FALSE) {
		// link error. print and die.
		GLchar error_log[1024];
		GLsizei length;
		glGetProgramInfoLog(shader_program, 1024, &length, error_log);
		cout << "Link error." << endl;
		cout << error_log << endl;
		cout << "Aborting due to link error." << endl;
		return false;
	}

	// PHEW! All ready to go! Push the button.
	glUseProgram(shader_program);

	setup_viewport();

	return true;
}

void setup_viewport() {
	// (and also the shader a little bit)
	// a bunch of this we have to call at least once right off the bat, so it's in here.

	// set some default colors - we should never see them, but if we do we know something's wrong
	glClearColor(255, 255, 255, 1.0);
	glColor3d(0, 0, 0);

	// feed it a projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, viewport_width, 0, viewport_height); // this is fine because I deal with the projection later!

	// and set it back to normal
	glMatrixMode(GL_MODELVIEW);

	// not managing the depth buffer has led to lots of segfaults.
	// at least I think that's why.
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// fill uniforms - size, scale, and time elapsed in millis
	GLint size_unif = glGetUniformLocation(shader_program, "size"); // send the resolution to the shader
	glUniform2f(size_unif, viewport_width, viewport_height);

	GLint scale_unif = glGetUniformLocation(shader_program, "scale");
	glUniform1f(scale_unif, scale);

	GLint time_unif = glGetUniformLocation(shader_program, "time");
	glUniform1f(time_unif, animation_time_base);

	GLint position_unif = glGetUniformLocation(shader_program, "position");
	glUniform2f(position_unif, pan_x, pan_y);
}

////////////////////////////////////////////// glut handlers
void reshape_handler(GLint new_x, GLint new_y) {
	// keep globals in sync
	viewport_width = new_x;
	viewport_height = new_y;

	// unsure as to exactly how much I have to do here.
	glViewport(0, 0, viewport_width, viewport_height);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // not managing the depth buffer has led to lots of segfaults.

	// fix up the projection
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, viewport_width, 0, viewport_height); // this is fine because I deal with the projection later!

	// and revert to normal
	glMatrixMode(GL_MODELVIEW);

	// update uniforms that changed
	GLint size_unif = glGetUniformLocation(shader_program, "size"); // send the new resolution to the shader
	glUniform2f(size_unif, viewport_width, viewport_height);
}

void display_handler() {
	// out of time, but this should be reachitected completely. Reset should be a case of zoom, and it should always be zooming towards a
	// scale value and coordinates at a speed.
	float target_x, target_y;
	timeval current;
	gettimeofday(&current, NULL);

	if(zooming) {
		scale = 0.0 + current.tv_sec + current.tv_usec / 1000000.0 - animation_starting_time;
		if(scale >= zoom_stop) zooming = false;
	}

	if(resetting) {
		// a decidedly poor way to to this, but it works. Just regrab the zoom-to coords. :(
		switch(queued_animation) {
			case 1:
				target_x = -1.74981;
				target_y = 0.0;
				break;
			case 2:
				target_x = -.745;
				target_y = 0.186; 
				break;
			case 3:
				target_x = -0.08896;
				target_y = 0.65405;
				break;
			default:
				target_x = -1.0;
				target_y = 0.0;
				break;
		}

		clock_t clock_val = clock(); // I'm hoping by moving this call out the compiler will optimize most of the following
	
		// avert your eyes! this block is a disaster. I had so many type issues I gave up and decided to let the compiler do /everything/
		scale -= ((scale - 1.0) * (clock_val - last_frame_time) / (CLOCKS_PER_SEC/25.0)) * (current.tv_sec + current.tv_usec / 1000000.0 - animation_starting_time);
		if ((scale - 1.0) * (clock_val - last_frame_time) / (CLOCKS_PER_SEC/25.0)* (current.tv_sec + current.tv_usec / 1000000.0 - animation_starting_time)/1.0 < .001) {
			// do these starting halfway, and at twice the speed for better effect (less zoom through black)
			pan_x -= (pan_x - target_x) * (clock_val - last_frame_time) / (CLOCKS_PER_SEC/15.0) * (current.tv_sec + current.tv_usec / 1000000.0 - animation_starting_time)/1.0;
			pan_y -= (pan_y - target_y) * (clock_val - last_frame_time) / (CLOCKS_PER_SEC/15.0) * (current.tv_sec + current.tv_usec / 1000000.0 - animation_starting_time)/1.0;
		}
	
		// check if we're close enough to cheat, otherwise it'll run forever (well, down to floating point precision), due to the exponential style decay here.
		if ((scale - 1.0)      * (clock_val - last_frame_time) / (CLOCKS_PER_SEC/25.0) * (current.tv_sec + current.tv_usec / 1000000.0 - animation_starting_time)/1.0 < .000001 &&
			(pan_x - target_x) * (clock_val - last_frame_time) / (CLOCKS_PER_SEC/15.0) * (current.tv_sec + current.tv_usec / 1000000.0 - animation_starting_time)/1.0 < .0001 &&
			(pan_y - target_y) * (clock_val - last_frame_time) / (CLOCKS_PER_SEC/15.0) * (current.tv_sec + current.tv_usec / 1000000.0 - animation_starting_time)/1.0 < .0001) {
			// this is close enough to the end (yes, the speed scales with window size :( )
			resetting = false; scale=1.0; pan_x = target_x; pan_y = target_y;
			switch(queued_animation) { // then launch the selected animation
				case 1:
					loc1();
					break;
				case 2:
					loc2();
					break;
				case 3:
					loc3();
					break;
				default:
					break;
			}
		}
		last_frame_time =  clock();

	}

	// then update uniforms
	GLint scale_unif = glGetUniformLocation(shader_program, "scale");
	glUniform1f(scale_unif, scale);

	// feed in elapsed time since viewport setup
	GLint time_unif = glGetUniformLocation(shader_program, "time");
	glUniform1f(time_unif, current.tv_sec + current.tv_usec / 1000000.0 - animation_time_base);
		
	GLint position_unif = glGetUniformLocation(shader_program, "position");
	glUniform2f(position_unif, pan_x, pan_y);

	// draw one, screenfilling rect, then trust in the shader! you can do it little buddy
    glRecti(0, 0, viewport_width, viewport_height);
	glutSwapBuffers();
}

void mouse_handler(GLint button, GLint state, GLint pos_x, GLint pos_y) {
	// to be continued...
	// ...evidently not.
}

void keyboard_handler(unsigned char key, GLint pos_x, GLint pos_y) {
	// calculate time in case we need it
	timeval current;
	gettimeofday(&current, NULL);
	suseconds_t current_time = current.tv_usec;

	// okay, so the sec * usec/10^6 metric is actually meaningless due to the types
	// but, clock() isn't precise enough for smooth animation
	// this isn't worth fixing. worst case is it hangs for a second waiting for the microsec count to roll over

	int adjustment = 1.0;
	if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
		adjustment = 0.2;
	else if(glutGetModifiers() == GLUT_ACTIVE_CTRL)
		adjustment = 3.0;

	switch(key) {
		// zooms
		case '=':
			scale += .1*adjustment;
			break;
		case '-':
			scale -= .1*adjustment;
			break;
		case '`': // palette switch
			// nested call just so we don't declare anything new inside the switch. definitely not safe
			animating = (animating == 1.0) ? 0.0 : 1.0; // flip value
			glUniform1f(glGetUniformLocation(shader_program, "animate"), animating); // then feed it to the shader, 1.0 = true
			break;
		// animations
		case '1':
			queued_animation = 1;
			reset();
			break;
		case '2':
			queued_animation = 2;
			reset();
			break;
		case '3':
			queued_animation = 3;
			reset();
			break;
		case 'r':
			queued_animation = -1;
			animation_starting_time = current.tv_sec + current.tv_usec / 1000000.0 ; 
			last_frame_time = clock();
			zooming = false; // otherwise they can fight. it's actually kind of fun
			resetting = true;
			break;
		case ' ': // break
			zooming = false;
			resetting = false;
			break;
		default:
			break;
	}

	// then update uniforms
	GLint scale_unif = glGetUniformLocation(shader_program, "scale");
	glUniform1f(scale_unif, scale);
	
	GLint position_unif = glGetUniformLocation(shader_program, "position");
	glUniform2f(position_unif, pan_x, pan_y);
	
	// and request an update right away so it's responsive.
	glutPostRedisplay();
}

void keyboard_special_handler(int key, GLint pos_x, GLint pos_y) {
	// move at a fifth speed for shift, triple for control
	float adjustment = 1.0;
	if(glutGetModifiers() == GLUT_ACTIVE_SHIFT)
		adjustment = 0.2;
	else if(glutGetModifiers() == GLUT_ACTIVE_CTRL)
		adjustment = 3.0;
	
	switch(key) {
		case GLUT_KEY_UP:
			pan_y += .02*(1.0/(scale*scale))*adjustment;
			break;
		case GLUT_KEY_DOWN:
			pan_y -= .02*(1.0/(scale*scale))*adjustment;
			break;
		case GLUT_KEY_LEFT:
			pan_x -= .02*(1.0/(scale*scale))*adjustment;
			break;
		case GLUT_KEY_RIGHT:
			pan_x += .02*(1.0/(scale*scale))*adjustment;
			break;
		default:
			break;
	}

	// then update uniforms
	GLint position_unif = glGetUniformLocation(shader_program, "position");
	glUniform2f(position_unif, pan_x, pan_y);
	
	// and request an update right away so it's responsive.
	glutPostRedisplay();
}

void idle_handler() {
	// if it's just sitting around, may as well ask for another frame.
	glutPostRedisplay();
}

////////////////////////////////////////////// sighseeing locations
void loc1() {
	timeval current;
	gettimeofday(&current, NULL);

	pan_x = -1.74981;
	pan_y = 0.0;
	animation_starting_time = current.tv_sec + current.tv_usec / 1000000.0 ; 
	scale = 1.0;
	zooming = true;
	resetting = false;
	zoom_stop = 7.8;
}

void loc2() {
	timeval current;
	gettimeofday(&current, NULL);

	pan_x = -.745;
	pan_y = 0.186;
	animation_starting_time = current.tv_sec + current.tv_usec / 1000000.0 ; 
	scale = 1.0;
	zooming = true;
	resetting = false;
	zoom_stop = 5.0;
}

void loc3() {
	timeval current;
	gettimeofday(&current, NULL);

	pan_x = -0.08896;
	pan_y = 0.65405;
	animation_starting_time = current.tv_sec + current.tv_usec / 1000000.0 ;
	scale = 1.0;
	zooming = true;
	resetting = false;
	zoom_stop = 6.0;
}

void reset() {
	timeval current;
	gettimeofday(&current, NULL);

	animation_starting_time = current.tv_sec + current.tv_usec / 1000000.0 ; 
	last_frame_time = clock();
	zooming = false; // otherwise they can fight. it's actually kind of fun
	resetting = true;
}
